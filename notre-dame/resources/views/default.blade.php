<!DOCTYPE html>
<html>

<head>

    <!-- Basic -->
    <meta charset="utf-8">
    <title>Notre Dame - Planos Médicos</title>
    <meta name="keywords" content="HTML5 Template"/>
    <meta name="description" content="Notre Dame - Planos Médicos">
    <meta name="author" content="okler.net">

    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Web Fonts  -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800|Alegreya|Alegreya+SC|Oswald:400,300"
          rel="stylesheet"
          type="text/css">

    <!-- Vendor CSS -->
    <link rel="stylesheet" href="/vendor/bootstrap/bootstrap.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css"
          integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ"
          crossorigin="anonymous">
    <link rel="stylesheet" href="/vendor/rs-plugin/css/settings.css">
    <link rel="stylesheet" href="/vendor/owlcarousel/owl.carousel.css">
    <link rel="stylesheet" href="/vendor/owlcarousel/owl.theme.css">
    <link rel="stylesheet" href="/vendor/magnific-popup/magnific-popup.css">

    <!-- Theme CSS -->
    <link rel="stylesheet" href="/css/theme.css">
    <link rel="stylesheet" href="/css/theme-elements.css">
    <link rel="stylesheet" href="/css/theme-blog.css">
    <link rel="stylesheet" href="/css/theme-shop.css">
    <link rel="stylesheet" href="/css/theme-animate.css">


    <!-- Skin CSS -->
    <link rel="stylesheet" href="/css/skins/default.css">

    <!-- Theme Custom CSS -->
    <link rel="stylesheet" href="/css/custom.css?{{ rand(0,9999) }}">

    <!-- Head Libs -->
    <script src="/vendor/modernizr/modernizr.js"></script>

    <!--[if IE]>
    <link rel="stylesheet" href="/css/ie.css">
    <![endif]-->

    <!--[if lte IE 8]>
    <script src="/vendor/respond/respond.js"></script>
    <script src="/vendor/excanvas/excanvas.js"></script>
    <![endif]-->

    <link href="/jivosite/jivosite.css?{{ rand(0,9999) }}" rel="stylesheet" />
    <script src="/jivosite/jivosite.js" type="text/javascript"></script>

</head>
<body>
<div class="body">
    @include('skeleton.header')

    <div role="main" class="main">
        @yield('content')

        @include('skeleton.orcamento', $configs)

        @yield('chamada')
    </div>
    @include('skeleton.footer', $configs)
</div>

<!-- Vendor -->
<script src="/vendor/jquery/jquery.js"></script>
<script src="/vendor/jquery.appear/jquery.appear.js"></script>
<script src="/vendor/jquery.easing/jquery.easing.js"></script>
<script src="/vendor/jquery-cookie/jquery-cookie.js"></script>
<script src="/vendor/bootstrap/bootstrap.js"></script>
<script src="/vendor/common/common.js"></script>
<script src="/vendor/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
<script src="/vendor/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
<script src="/vendor/jquery.validation/jquery.validation.js"></script>
<script src="/vendor/jquery.stellar/jquery.stellar.js"></script>
<script src="/vendor/jquery.easy-pie-chart/jquery.easy-pie-chart.js"></script>
<script src="/vendor/jquery.gmap/jquery.gmap.js"></script>
<script src="/vendor/twitterjs/twitter.js"></script>
<script src="/vendor/isotope/jquery.isotope.js"></script>
<script src="/vendor/owlcarousel/owl.carousel.js"></script>
<script src="/vendor/jflickrfeed/jflickrfeed.js"></script>
<script src="/vendor/magnific-popup/jquery.magnific-popup.js"></script>
<script src="/vendor/vide/vide.js"></script>

<!-- Theme Base, Components and Settings -->
<script src="/js/theme.js"></script>

<!-- Specific Page Vendor and Views -->
<script src="/js/views/view.contact.js"></script>

<!-- Theme Custom -->
<script src="/js/custom.js"></script>

<!-- Theme Initialization Files -->
<script src="/js/theme.init.js"></script>

<!-- BEGIN JIVOSITE CODE {literal} -->
<script type='text/javascript'>
    (function(){ var widget_id = 'fDYdTEo6aH';var d=document;var w=window;function l(){var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true;s.src = '//code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);}if(d.readyState=='complete'){l();}else{if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}}})();
</script>
<!-- {/literal} END JIVOSITE CODE -->


<!-- Google Analytics: Change UA-XXXXX-X to be your site's ID. Go to http://www.google.com/analytics/ for more information.
<script type="text/javascript">

    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-12345678-1']);
    _gaq.push(['_trackPageview']);

    (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();

</script>
 -->
</body>
</html>