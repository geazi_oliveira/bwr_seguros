<div class="container">

    <div class="row">
        <div class="col-md-6">

            <div class="alert alert-success hidden" id="contactSuccess">
                <strong>Pronto!</strong> Obrigado, sua mensagem foi enviada para nós.
            </div>

            <div class="alert alert-danger hidden" id="contactError">
                <strong>Ops!</strong> Houve algum erro ao recebermos sua mensagem, tente mais tarde.
            </div>

            <h2 class="short">
                <strong>ORÇAMENTO</strong> POR E-MAIL
            </h2>
            <form id="contactForm" action="http://18.231.172.118:8081/send" method="POST">
                <input type="hidden" id="from" value="{{ $site }}">
                <div class="row">
                    <div class="form-group">
                        <div class="col-md-6">
                            <label>Seu Nome *</label>
                            <input type="text" value="" data-msg-required="Por favor entre com seu nome."
                                   maxlength="100" class="form-control" name="name"
                                   id="name" required>
                        </div>
                        <div class="col-md-6">
                            <label>Seu Email *</label>
                            <input type="email" value="" data-msg-required="Por favor entre com seu endereço de email."
                                   data-msg-email="Por favor entre com um endereço de email valído."
                                   maxlength="100" class="form-control" name="email" id="email" required>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group">
                        <div class="col-md-6">
                            <label>Telefone Residencial *</label>
                            <input type="text" value="" data-msg-required="Por favor entre com seu telefone."
                                   maxlength="100" class="form-control" name="phone"
                                   id="phone" required>
                        </div>
                        <div class="col-md-6">
                            <label>Telefone Celular *</label>
                            <input type="text" value="" data-msg-required="Por favor entre com seu celular."
                                   maxlength="100" class="form-control" name="celphone"
                                   id="celphone" required>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group">
                        <div class="col-md-12">
                            <label>Mensagem *</label>
                            <textarea maxlength="5000" data-msg-required="Por favor entre co sua mensagem." rows="10"
                                      class="form-control" name="message"
                                      id="message" required></textarea>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <input type="submit" value="Enviar" class="btn btn-primary btn-lg"
                               data-loading-text="Carregando...">
                    </div>
                </div>
            </form>
        </div>
        <div class="col-md-6">

            <h4 class="push-top">
                Entre em
                <strong>contato</strong>
            </h4>
            <p>
                Temos equipes de atendimento especializada que está pronta para realizar uma consultoria de qualidade
                identificando o melhor
                plano para sua empresa.
            </p>

            <hr/>

            <h4>
                <strong>Escritório</strong>
            </h4>
            <ul class="list-unstyled">
                <li>
                    <i class="fa fa-map-marker"></i>
                    <strong>Endereço:</strong> Rua Itapura, 239 conjunto 503 - Tatuapé São Paulo
                </li>
                @foreach($phones as $phone)
                    <li>
                        <i class="{{ $phone['icon'] }}"></i>
                        <strong>{{ $phone['title'] }}:</strong> {{ $phone['phone'] }}
                    </li>
                @endforeach
                <li>
                    <i class="fa fa-envelope"></i>
                    <strong>Email:</strong>
                    <a href="mailto:{{ $email }}">
                        {{ $email }}
                    </a>
                </li>
            </ul>

        </div>

    </div>

</div>