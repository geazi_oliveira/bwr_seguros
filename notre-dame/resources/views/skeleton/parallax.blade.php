<section class="parallax" data-stellar-background-ratio="0.4" style="background-image: url(/img/parallax-2.jpg);">
<div class="container">
    <div class="row">
        <div class="col-md-6" data-appear-animation="fadeInLeft">
            <h2 class="push-top">ORÇAMENTO
                <strong>ONLINE</strong>
            </h2>
            <p class=" tall">
                Temos equipes de atendimento especializada que está pronta para realizar uma consultoria de
                qualidade identificando o melhor plano para sua empresa.
            </p>
            <a class="btn btn-primary btn-lg push-top" href="/orcamento">Fazer Orçamento...</a>
        </div>
        <div class="right-side col-md-5 col-md-offset-1" data-appear-animation="fadeInRight" data-appear-animation-delay="300">
            <img class="img-responsive pull-right" src="/img/home/contrate-um-plano.png">
        </div>
    </div>
</div>
</section>