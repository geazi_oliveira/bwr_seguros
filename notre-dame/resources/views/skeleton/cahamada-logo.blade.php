<div class="container">

    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-3">
                    <div class="feature-box-image">
                        <img src="/img/home/img-01.jpg">
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="feature-box-image">
                        <img src="/img/home/img-02.jpg">
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="feature-box-image">
                        <img src="/img/home/img-03.png">
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="feature-box-image">
                        <img src="/img/home/img-04.jpg">
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>