<div class="container">
    <section class="call-to-action featured footer">
        <div class="container">
            <div class="row">
                <div class="center">
                    <h3>Venha conheçer a
                        <strong>{{ $empresa }}</strong>, tudo que a sua saúde
                        <strong>precisa</strong>
                        <a href="/orcamento" class="btn btn-lg btn-primary appear-animation bounceIn appear-animation-visible"
                           data-appear-animation="bounceIn">Faça uma cotação agora!</a>
                        <span class="arrow hlb hidden-xs hidden-sm hidden-md appear-animation" data-appear-animation="rotateInUpLeft" style="top: -22px;"></span>
                    </h3>
                </div>
            </div>
        </div>
    </section>
</div>