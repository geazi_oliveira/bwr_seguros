@extends('default')
@section('content')
    @include('skeleton.breadcrumb', ['name' => 'Plano de Empresarial'])

    @include('skeleton.cahamada-logo')

    @include('skeleton.parallax')

    <div class="container">

        <div class="row">

            <div data-appear-animation="fadeInDown" class="appear-animation">
                <div class="row featured-boxes secundary">
                    <div class="col-md-2">
                        <div data-appear-animation="bounceIn" data-appear-animation-delay="600"
                             class="appear-animation">
                            <div class="featured-box featured-box-primary">
                                <div class="box-content">
                                    <i class="icon-featured fa fa-check"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-10">
                        <h3 class="short dark">Smart</h3>
                        <p>
                            A Linha Smart é ideal para quem deseja fazer uma escolha racional do plano de saúde, que
                            ofereça atendimento integral à saúde, com a qualidade esperada e uma rede de referência
                            de grande abrangência regional.
                        </p>
                    </div>
                </div>
            </div>

            <div data-appear-animation="fadeInDown" class="appear-animation">
                <div class="row">
                    <div data-appear-animation="fadeIn" data-appear-animation-delay="800" class="appear-animation">
                        <div class="col-md-2 center push-bottom">
                            <span class="icon-rounded">
                                <i class="fas fa-plus"></i>
                            </span>
                        </div>
                    </div>
                </div>
            </div>

            <div data-appear-animation="fadeInDown" class="appear-animation">
                <div class="row featured-boxes secundary">
                    <div class="col-md-2">
                        <div data-appear-animation="bounceIn" data-appear-animation-delay="600"
                             class="appear-animation">
                            <div class="featured-box featured-box-primary">
                                <div class="box-content">
                                    <i class="icon-featured fa fa-check"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-10">
                        <h3 class="short dark">Advance</h3>
                        <p>
                            A Linha Advance oferece planos de saúde com cobertura nacional e acesso à uma ampla Rede
                            Própria certificada e credenciada de hospitais e laboratórios e profissionais credenciados
                            em todas as especialidades médicas.
                        </p>
                    </div>
                </div>
            </div>

            <div data-appear-animation="fadeInDown" class="appear-animation">
                <div class="row">
                    <div data-appear-animation="fadeIn" data-appear-animation-delay="800" class="appear-animation">
                        <div class="col-md-2 center push-bottom">
                            <span class="icon-rounded">
                                <i class="fas fa-plus"></i>
                            </span>
                        </div>
                    </div>
                </div>
            </div>

            <div data-appear-animation="fadeInDown" class="appear-animation">
                <div class="row featured-boxes secundary">
                    <div class="col-md-2">
                        <div data-appear-animation="bounceIn" data-appear-animation-delay="600"
                             class="appear-animation">
                            <div class="featured-box featured-box-primary">
                                <div class="box-content">
                                    <i class="icon-featured fa fa-check"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-10">
                        <h3 class="short dark">Premium</h3>
                        <p>
                            A Linha Premium abrange planos de saúde com atendimento de extrema qualidade e coberturas
                            diversificadas. Assim, você tem a liberdade de escolher a melhor opção para as suas
                            necessidades.
                        </p>
                    </div>
                </div>
            </div>

            <div data-appear-animation="fadeInDown" class="appear-animation">
                <div class="row">
                    <div data-appear-animation="fadeIn" data-appear-animation-delay="800" class="appear-animation">
                        <div class="col-md-2 center push-bottom">
                            <span class="icon-rounded">
                                <i class="fas fa-plus"></i>
                            </span>
                        </div>
                    </div>
                </div>
            </div>

            <div data-appear-animation="fadeInDown" class="appear-animation">
                <div class="row featured-boxes secundary">
                    <div class="col-md-2">
                        <div data-appear-animation="bounceIn" data-appear-animation-delay="600"
                             class="appear-animation">
                            <div class="featured-box featured-box-primary">
                                <div class="box-content">
                                    <i class="icon-featured fa fa-check"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-10">
                        <h3 class="short dark">Infinity</h3>
                        <p>
                            A Linha Infinity oferece os melhores serviços, coberturas e programas de prevenção de
                            doenças,
                            com atendimento privilegiado em todo o País.
                        </p>
                    </div>
                </div>
            </div>

        </div>
        <hr class="tall"/>
    </div>

@endsection