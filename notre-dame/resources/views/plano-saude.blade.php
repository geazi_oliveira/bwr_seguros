@extends('default')
@section('content')
    @include('skeleton.breadcrumb', ['name' => 'Plano de Saúde'])

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2>Linha Smart</h2>

                <p>
                    A Linha Smart é ideal para quem deseja fazer uma escolha racional do plano de saúde, que ofereça
                    atendimento integral à saúde, com a qualidade esperada e uma rede de referência de grande
                    abrangência regional. Você pode contratá-lo com ou sem coparticipação, resultando no melhor
                    custo-benefício.
                </p>

            </div>
        </div>
        <hr class="tall"/>
        <div class="row">
            <div class="col-md-12">
                <h2>Porque escolher os planos Notre Dame?</h2>

                <p>
                    Mais de 3,2 milhões de beneficiários e mais de 5 mil empresas<br/>
                    59 Centros Clínicos, sendo 9 exclusivamente de Medicina Preventiva, 14 prontos-socorros, 11
                    hospitais e 8 maternidades
                </p>

            </div>
        </div>
        <hr class="tall"/>
    </div>

@endsection

@section('chamada')
    @include('skeleton.chamada', ['empresa' => 'Notre Dame'])
@endsection