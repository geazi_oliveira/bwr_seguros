<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$configs = [
    'site' => 'Notre Dame Intermédica',
    'email' => 'vendas@intermedicaplanosempresas.com.br',
    'address' => 'Rua Itapura, 239 conjunto 503 - Tatuapé São Paulo',
    'phones' => [
        [
            'icon' => 'fa fa-phone',
            'phone' => '(11) 4117-3779',
            'title' => 'Telefone'
        ],
        [
            'icon' => 'fa fa-phone',
            'phone' => '0800 580 23 87',
            'title' => 'Telefone'
        ],
        [
            'icon' => 'fab fa-whatsapp',
            'phone' => '(11) 95433-5433',
            'title' => 'WhatsApp'
        ],

    ]
];

$router->get('/', function () use ($configs) {
    return view('index', ['configs' => $configs]);
});

$router->get('plano-de-saude', function () use ($configs) {
    return view('plano-saude', ['configs' => $configs]);
});

$router->get('plano-empresarial', function () use ($configs) {
    return view('plano-empresarial', ['configs' => $configs]);
});


$router->get('orcamento', function () use ($configs) {
    return view('orcamento', ['configs' => $configs]);;
});
