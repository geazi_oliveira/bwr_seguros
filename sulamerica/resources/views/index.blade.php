@extends('default')
@section('content')
    <section class="home-top clean">
        <div class="slider-container">
            <div class="slider" id="revolutionSlider" data-plugin-revolution-slider data-plugin-options='{"startheight": 580}'>
                <ul>
                    <li data-transition="fade" data-slotamount="13" data-masterspeed="300">
                        <img src="/img/slides/banner1.png" data-bgposition="left top" data-bgrepeat="no-repeat">
                    </li>
                    <li data-transition="fade" data-slotamount="13" data-masterspeed="300">
                        <img src="/img/slides/banner2.jpg" data-bgposition="left top" data-bgrepeat="no-repeat">
                    </li>
                </ul>
            </div>
        </div>
    </section>

    @include('skeleton.cahamada-logo')

    @include('skeleton.parallax')

    @include('skeleton.planos')

@endsection