@extends('default')
@section('content')
    @include('skeleton.breadcrumb', ['name' => 'Planos de Saúde'])

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2>Hospitalar</h2>

                <p>
                    Urgências e emergências, internações hospitalares e partos. Consultas e exames são cobertos exclusivamente para gestantes, em acompanhamento pré-natal.
                </p>
                <p>
                    Hemodiálises, quimioterapias, radioterapias e fisioterapias são cobertas quando relacionadas à continuidade da assistência prestada em nível de internação hospitalar.
                </p>

            </div>
        </div>
        <hr class="tall"/>
        <div class="row">
            <div class="col-md-12">
                <h2>Ambulatorial e Hospitalar com Obstetrícia</h2>

                <p>
                    Consultas médicas, urgências e emergências, exames, cirurgias, internações hospitalares e partos.
                </p>

            </div>
        </div>
        <hr class="tall"/>
    </div>

@endsection

@section('chamada')
    @include('skeleton.chamada', ['empresa' => 'SulAmerica'])
@endsection