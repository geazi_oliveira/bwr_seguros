@extends('default')
@section('content')
    @include('skeleton.breadcrumb', ['name' => 'Planos de Empresariais'])

    @include('skeleton.cahamada-logo')

    @include('skeleton.parallax')

    @include('skeleton.planos')

@endsection