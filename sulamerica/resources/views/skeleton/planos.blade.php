<div class="container">

    <div class="row">

        <div data-appear-animation="fadeInDown" class="appear-animation">
            <div class="row featured-boxes secundary">
                <div class="col-md-2">
                    <div data-appear-animation="bounceIn" data-appear-animation-delay="600"
                         class="appear-animation">
                        <div class="featured-box featured-box-primary">
                            <div class="box-content">
                                <i class="icon-featured fa fa-check"></i>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-10">
                    <h3 class="short dark">SulAmérica Saúde PME</h3>
                    <p>
                        Para empresas e empresários, a partir de 1 titular e 2 dependentes, com melhores preços e
                        atendimento nacional, sem coparticipação
                    </p>
                </div>
            </div>
        </div>

        <div data-appear-animation="fadeInDown" class="appear-animation">
            <div class="row">
                <div data-appear-animation="fadeIn" data-appear-animation-delay="800" class="appear-animation">
                    <div class="col-md-2 center push-bottom">
                            <span class="icon-rounded">
                                <i class="fas fa-plus"></i>
                            </span>
                    </div>
                </div>
            </div>
        </div>

        <div data-appear-animation="fadeInDown" class="appear-animation">
            <div class="row featured-boxes secundary">
                <div class="col-md-2">
                    <div data-appear-animation="bounceIn" data-appear-animation-delay="600"
                         class="appear-animation">
                        <div class="featured-box featured-box-primary">
                            <div class="box-content">
                                <i class="icon-featured fa fa-check"></i>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-10">
                    <h3 class="short dark">SulAmérica Saúde PME MAIS</h3>
                    <p>
                        Para empresas de 30 a 99 usuários, totalmente isento de carências
                    </p>
                </div>
            </div>
        </div>

        <div data-appear-animation="fadeInDown" class="appear-animation">
            <div class="row">
                <div data-appear-animation="fadeIn" data-appear-animation-delay="800" class="appear-animation">
                    <div class="col-md-2 center push-bottom">
                            <span class="icon-rounded">
                                <i class="fas fa-plus"></i>
                            </span>
                    </div>
                </div>
            </div>
        </div>

        <div data-appear-animation="fadeInDown" class="appear-animation">
            <div class="row featured-boxes secundary">
                <div class="col-md-2">
                    <div data-appear-animation="bounceIn" data-appear-animation-delay="600"
                         class="appear-animation">
                        <div class="featured-box featured-box-primary">
                            <div class="box-content">
                                <i class="icon-featured fa fa-check"></i>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-10">
                    <h3 class="short dark">SulAmérica Saúde Empresarial</h3>
                    <p>
                        Para empresas com 100 usuários ou acima
                    </p>
                </div>
            </div>
        </div>

        <div data-appear-animation="fadeInDown" class="appear-animation">
            <div class="row">
                <div data-appear-animation="fadeIn" data-appear-animation-delay="800" class="appear-animation">
                    <div class="col-md-2 center push-bottom">
                            <span class="icon-rounded">
                                <i class="fas fa-plus"></i>
                            </span>
                    </div>
                </div>
            </div>
        </div>

        <div data-appear-animation="fadeInDown" class="appear-animation">
            <div class="row featured-boxes secundary">
                <div class="col-md-2">
                    <div data-appear-animation="bounceIn" data-appear-animation-delay="600"
                         class="appear-animation">
                        <div class="featured-box featured-box-primary">
                            <div class="box-content">
                                <i class="icon-featured fa fa-check"></i>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-10">
                    <h3 class="short dark">Para Pessoas</h3>
                    <p>
                        Entre em contato conosco para ter maiores informações.
                    </p>
                </div>
            </div>
        </div>

    </div>
    <hr class="tall"/>
</div>