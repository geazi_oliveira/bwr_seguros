<div class="container">

    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-3">
                    <div class="feature-box-image">
                        <img src="/img/sulamerica/coberturas.jpg">
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="feature-box-image">
                        <img src="/img/sulamerica/dental.jpg">
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="feature-box-image">
                        <img src="/img/sulamerica/empresarial.png">
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="feature-box-image">
                        <img src="/img/sulamerica/executivo.jpg">
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>