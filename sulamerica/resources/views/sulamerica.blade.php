@extends('default')
@section('content')
    @include('skeleton.breadcrumb', ['name' => 'A SulAmerica'])

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2>A SulAmerica</h2>

                <p>
                    <strong>Conheça um pouco da nossa empresa.</strong>
                </p>


                <p>O primeiro capítulo de nossa história tem como protagonista Dom Joaquim Sanchez de Larragoiti, um
                    homem de espírito empreendedor e cosmopolita, que fundou em dezembro de 1895 a Sul América Companhia
                    Nacional de Seguros de Vida. A empresa começou a operar no mercado brasileiro lançando diversos
                    produtos inovadores de seguros.
                </p>
                <p> 1897 – foi pago o primeiro sinistro à família do Sr. Antônio Augusto Portugal. Também neste ano, foi
                    criada a primeira Sucursal no exterior, em Buenos Aires.
                </p>
                <p>1913 – com a aquisição da Anglo Sul Americana, Sociedade de Seguros e Resseguros Terrestres e
                    Marítimos (que depois passou a chamar-se SATMA), a empresa começou a atuar também na área de seguros
                    terrestres e marítimos em 11 capitais, adequando-se às transformações industriais e urbanas por que
                    passava o país.
                </p>
                <p>1918 – lançou o Seguro de Sócios, Patrões e Empregados, a primeira forma de seguro de vida em grupo
                    do mercado brasileiro.
                </p>
                <p>Década de 20 – lançou os Seguros de Automóveis, de Acidentes Pessoais e o Sistema de Capitalização.
                </p>
                <p>1928, por exemplo, adota o pagamento do 13º salário, antes mesmo que tal benefício fosse obrigatório
                    por lei. Desde os primeiros anos de sua existência, a Sul América estabeleceu um novo conceito de
                    relação com os funcionários.
                </p>
                <p>1929 -a diversificação de atividades ajudou o Grupo a superar as dificuldades da crise de 1929.
                </p>
                <p>1933 – doou recursos para a realização de obras de fornecimento de água para a cidade do Rio de
                    Janeiro. Consciente de sua responsabilidade social, a Sul América participa ativamente do
                    desenvolvimento urbano do país.
                </p>
                <p>1940 – a Sul América introduziu no país o seguro de responsabilidade civil.
                </p>
                <p>1944 – ampliou sua participação nas ações sociais desenvolvendo intensa campanha de erradicação da
                    difteria. Surpreendeu a todos, promovendo a distribuição de lucros entre seus funcionários.
                </p>
                <p>1949 – durante a inauguração do prédio da sucursal do Rio de Janeiro da SATMA, a Sul América promoveu
                    a maior exposição de arte moderna até então.
                </p>
                <p>Década de 40 – início dos investimentos da Sul América em construção de conjuntos residenciais e
                    moradias populares.
                </p>
                <p>1951 – tem início a construção do Hospital Larragoiti, atual Hospital da Lagoa, no Rio de Janeiro,
                    posteriormente doado à comunidade.
                </p>
                <p>1958 – a Sul América promoveu uma grande reforma administrativa, com o objetivo de reduzir as
                    despesas fixas, criar estímulos especiais aos funcionários e estabelecer uma organização em moldes
                    de macroempresa. Em 1964, a Lei nº 4.594, alterou a estrutura do processo de comercialização do
                    seguro, substituindo a sociedade seguradora pelo corretor.
                </p>
                <p>1968 – é criado um novo símbolo para a marca Sul América, o qual serviu de base para o
                    desenvolvimento da logomarca do Grupo. Na década de 70, a companhia dá início às atividades na área
                    de saúde, com a constituição do escritório de administração de Seguro Saúde.
                </p>
                <p>1977 – estabeleceu uma associação com a Gerling Konzern, da Alemanha, constituindo a Gerling Sul
                    América S.A. Seguros Industriais.
                </p>
                <p>1981 – criou a Sul América Serviços Médicos.
                </p>
                <p>1987 – deu início às operações da Sul América Previdência Privada S.A. (Sulaprevi)
                </p>
                <p>1989 – lança o Seguro Sul América Saúde, o primeiro do gênero no Brasil. No mesmo ano é lançado de
                    forma pioneira o Serviço de Assistência 24 Horas, sucesso seguido por diversas empresas do mercado.
                </p>
                <p>Década de 90 – marca a entrada da Sul América em seu segundo século: mais conquistas, novas
                    associações.
                </p>
                <p>1994 – começou a parceria com o Banco do Brasil, através da criação da Brasilprev, para a
                    comercialização de planos de Previdência Privada. Essa associação possibilitou à Sul América
                    utilizar, como canal de vendas, a maior rede de distribuição bancária do país.
                </p>
                <p>1995 – assumiu a liderança do mercado nacional de Seguro Saúde, ramo que mais cresceu no período. O
                    sucesso da associação com o Banco do Brasil se refletiu na criação das empresas BrasilCap,
                    BrasilSaúde e BrasilVeículos.
                </p>
                <p>A campanha de incentivo às vendas, realizada para os corretores, que culmina num grande evento no
                    Club Med, aconteceu pela primeira vez neste ano. Todos anos ela se repete, entre os meses de abril e
                    setembro. Como reconhecimento, os corretores que se destacaram são convidados a desfrutar de um
                    final de semana no Club Med, onde passarão por diversas atrações como: shows, campeonatos e sorteios
                    de carros, motos e eletroeletrônicos. Durante estes 10 anos de campanha, mais de dois mil corretores
                    de seguros participaram do evento e dezenas de automóveis foram entregues em sorteios.
                </p>
                <p>1996 – criou a SAGA – Sul América Gestão de Ativos (que posteriormente teve sua razão social alterada
                    para Sul América Investimentos), para atuar na administração de recursos de terceiros. A estratégia
                    de crescimento adotada pela companhia, aliada ao direcionamento para a constante melhoria de seus
                    produtos e serviços, levou-a a adquirir, do grupo Arbi, as seguradoras Santa Cruz Seguros S.A. e
                    Itatiaia Seguros S.A.
                </p>
                <p>Neste ano a SulAmérica começou as atividade do Programa de Estágio, que vigora até hoje.
                    Anualmente a SulAmérica abre espaço para que jovens talentos mostrem seu potencial e ingressem no
                    mercado de trabalho. O Programa de Estágio tem como objetivo desenvolver e formar profissionais
                    qualificados, possibilitando experiência prática em sua especialização profissional e conhecimento
                    da realidade empresarial. Além disso, a SulAmérica oferece, todos os anos, oportunidades para
                    portadores de deficiência física, para que desenvolvam suas habilidades e tenham uma oportunidade de
                    crescimento profissional.
                </p>
                <p>1997– a Sul América e mais quatro empresas seguradoras, formaram a Seguradora Brasileira de Crédito à
                    Exportação, primeira companhia do país destinada a operar exclusivamente no segmento de seguros para
                    o comércio externo. Ainda neste ano, a Sul América constituiu uma joint-venture com a Aetna Inc.,
                    empresa líder na área de seguro saúde e uma das maiores seguradoras norte-americanas. A Sul América
                    Aetna concentra suas atividades nos segmentos de Seguros de Saúde, Vida e Previdência.
                </p>
                <p>1998 – foi lançado o Sul América SOS USA. Com ele, o segurado passou a ter o direito ao atendimento
                    de emergência e urgência na rede referenciada da Aetna, em todo território americano. Outra grande
                    novidade: o seguro personalizado Sul América Auto, calculado de acordo com as características do
                    motorista, do veículo e com os riscos aos quais ele é exposto. No mesmo ano, a Sul América relançou
                    o seu site, uma versão muito mais completa, que oferecia mais serviços e informações, fazendo da
                    Internet um efetivo canal de relacionamento com a sociedade. Além disso, desenvolveu o NAC On-line,
                    um site voltado exclusivamente aos corretores de seguros, uma poderosa ferramenta de apoio a seus
                    parceiros na comercialização dos produtos da Sul América. O site oferece ao corretor instrumentos
                    que possam agilizar seu dia-a-dia e incentivar suas ações de gerenciamento e relacionamento com os
                    clientes. O Portal do Corretor, NAC Online já registrou mais de 96 mil acessos desde seu lançamento.
                </p>
                <p>1999 – o Projeto Aquarius, patrocinado pela SulAmérica e jornal O Globo, presenteou sua platéia com a
                    apresentação da Nona Sinfonia de Beethoven utilizando como cenário o Pão de Açúcar, um dos mais
                    bonitos cartões postais da cidade.
                </p>
                <p>2000 – quando o cantor e compositor Vinícius de Moraes completaria 87 anos de vida, a Sul América
                    produziu uma das maiores homenagens já feitas ao poeta. Toquinho, Roberto Menescal e Wanda Sá,
                    Emílio Santiago, Zimbo Trio e Os Cariocas, acompanhados pela Orquestra Sinfônica Brasileira,
                    interpretaram canções de Vinícius. A homenagem teve duas edições. A primeira aconteceu no Rio de
                    Janeiro, na praia de Ipanema reunindo cerca de 30 mil pessoas. Em São Paulo, o show foi realizado no
                    Parque do Ibirapuera, com 25 mil espectadores.
                    Neste ano a SulAmérica criou o Projeto Sul América de Saúde Ocular em parceria com a Organização
                    Não-Governamental (ONG) Helen Keler – com apoio do Hospital das Clínicas, Instituto Benjamin
                    Constant, Instituto Pró-Natura e as Secretarias de Educação, Ação Social e Saúde de São Paulo. O
                    objetivo é contribuir para a redução da cegueira infantil, além de visar o aumento do rendimento
                    escolar e o desenvolvimento social de crianças na faixa de 0 a 14 anos. O projeto realiza desde
                    testes de acuidade visual e exames oftalmológicos para a identificação de doenças visuais até a
                    cobertura de tratamentos clínicos e cirúrgicos, além da doação de óculos. Desde sua criação, o
                    projeto já beneficiou cerca de 100 mil crianças matriculadas em escolas na rede de ensino público
                    das cidades de São Paulo, Rio de Janeiro, Recife, Pernambuco, Bahia e Brasília.
                    A TV Executiva foi idealizada pela Sul América para servir como mais um canal de comunicação com os
                    corretores de seguro, seu maior parceiro de negócios há mais de cem anos. Por meio deste canal, a
                    companhia gerou em 2000 dois eventos no Rio – o Sul América On-Line com o Futuro e o Auto 2000 – que
                    foram transmitidos, via satélite, para outras 14 praças no país.
                    No segundo semestre de 2000, a Sul América Capitalização, se empenhou em reposicionar seus produtos
                    com o objetivo de criar um caráter de unidade. Desta ação surgiu a campanha Sul América Superfácil,
                    dando a todos os produtos de Capitalização da empresa esta classificação.
                    Como parte integrante de uma nova estratégia de comunicação, a Sul América lançou a revista Sul
                    América On-Line. Uma publicação idealizada e produzida pela própria companhia, que enfoca o impacto
                    dos avanços tecnológicos, em especial da internet, no dia-a-dia dos profissionais de seguros.
                    Dirigida a corretores, funcionários, colaboradores e prestadores de serviço da Sul América, a
                    revista anuncia ainda, as mudanças que a web está promovendo na relação empresa/cliente.
                </p>
                <p>2002 – o grupo financeiro holandês ING, assumiu 49% das ações da Sul América Aetna – braço do grupo
                    Sul América para as áreas de Saúde, Vida e Previdência. A operação foi concretizada com a compra da
                    área internacional e da administração de finanças da seguradora Aetna, sediada nos Estados Unidos,
                    que era acionista da companhia brasileira.
                </p>
                <p>2003 – A SulAmérica e o ING apresentam ao mercado a marca de sua associação. A identidade visual
                    ressalta todo o dinamismo, confiança, experiência e o lado socialmente responsável desta parceria e
                    os seus impactos positivos para o desenvolvimento do mercado de seguros, serviços financeiros e
                    previdência no país. A parceria, consolidada em março de 2002, foi um dos mais importantes
                    acontecimentos para o setor de seguros e de administração de ativos do país nos últimos anos. A
                    aquisição de 49% das ações da companhia brasileira pelo ING – uma das maiores instituições
                    financeiras e de seguros do mundo, com mais de 500 bilhões de dólares em ativos sob administração –
                    tem demonstrado, acima de tudo, a confiança destes dois grupos no potencial da economia nacional e
                    nas oportunidades de crescimento dos mercados considerados estratégicos para a associação.
                    Em nova versão, lançada em julho de 2003, o site Sul América se consolidou como um dos mais
                    importantes canais de informação, serviço e atendimento para os diversos públicos da companhia. Com
                    navegação facilitada e layout moderno, o novo site possibilita ao usuário conhecer as
                    características, benefícios e vantagens dos produtos da empresa em Seguros e Previdência, solicitar
                    cálculos, realizar consultas on-line e acessar informações corporativas
                    Neste ano a SulAmérica cria disponibilizou dois produtos de previdência completementar: o PGBL e o
                    VGBL.
                </p>
                <p>2004 – A SulAmérica, associada ao ING, lançou um produto inédito no mercado: o SulAmérica Previdência
                    Empresa Simplificado. Trata-se de um plano de previdência simples, com condições diferenciadas e
                    contribuições mais baixas que as existentes no mercado. Voltado para a micro, pequena e média
                    empresa, o produto oferece como vantagem a facilidade na hora do fechamento do negócio. O corretor
                    poderá preencher na própria empresa a proposta de estudo já pré-aprovada e enviar os dados para a
                    SulAmérica, economizando 30 dias, tempo que normalmente demora para a conclusão da operação.
                </p>
                <p>Primeira no mercado a oferecer serviços de Assistência 24 horas, a SulAmérica sai na frente mais uma
                    vez, lançando o Auto Pop, destinado para as pessoas que possuem veículos com mais de quatro anos de
                    idade, oferecendo preços menores que os tradicionais. A cobertura oferecida de colisão e incêndio
                    tem o valor com cerca de 40% mais baixo do que a cobertura normal, para os veículos que tenham entre
                    5 e 15 anos de fabricação.
                </p>
                <p>A área de Recursos Humanos da SulAmérica implantou o Programa de Gestão de Desempenho e
                    Desenvolvimento (PGDD). O programa consiste na avaliação das competências essenciais dos
                    funcionários da empresa e dos resultados alcançados no período. Além disso, é realizada a elaboração
                    de um plano de desenvolvimento e planejamento dos objetivos para o ano subseqüente.
                </p>
                <p>Em 2004, a SulAmérica Investimentos ficou entre as dez instituições financeiras que mais captaram no
                    mercado, com um crescimento acima da média. Só no final deste ano foram registrados R$ 6 bilhões em
                    recursos administrativos, expansão de 19,38% considerando o mesmo período de 2003. Esse é mais um
                    resultado positivo obtido pela empresa, destacando como diferencial o serviço oferecido de alta
                    qualidade e a ótima performance dos nossos fundos, destacando o SulAmérica Multicarteira FIF (fundo
                    balanceado que atua nos mercados de renda fixa e variável), SulAmérica Hedge FIF (fundo que adota
                    estratégia de investimentos em vários mercados) e SulAmérica Multimercado Dinâmico FIF (ganho pelo
                    aproveitamento de distorções nos preços dos ativos negociados nas Bolsas de Valores e de Futuros).
                </p>
                <p>2005 – Lançado este ano nas cidades de Ribeirão Preto e Campinas, o SulAmérica Consórcio oferece
                    planos para aquisição de imóvel, caminhão e automóvel. O lançamento está alinhado à estratégia da
                    SulAmérica, em parceria com o ING, de consolidar-se como empresa de serviços financeiros no mercado
                    brasileiro. A comercialização do SulAmérica Consórcio é efetuada, exclusivamente, por corretores de
                    seguros, principais parceiros de negócios da companhia.
                </p>
                <p>A UNIVERSAS – Universidade Corporativa da SulAmérica, foi lançada no primeiro semestre. É um canal
                    virtual de treinamento focado no aprimoramento de competências profissionais dos funcionários e dos
                    corretores da SulAmérica. Concentra iniciativas de desenvolvimento e educação contínua a partir das
                    estratégias da empresa. São oferecidas oportunidades de treinamento online acompanhados por tutores,
                    que respondem dúvidas por e-mail. Além disso, a universidade oferece espaços para fóruns, chats,
                    divulgações a partir de murais eletrônicos e pesquisas de opinião. Por meio da UNIVERSAS para
                    Corretores, a empresa pretende qualificar ainda mais seus parceiros e ampliar a rede de ditribuição,
                    composta hoje por cerca de 25 mil profissionais.
                </p>
                <p>Para o público jovem, a SulAmérica lançou um plano de previdência: o Educaprevi. Desenhado
                    especialmente para os pais que querem garantir o estudo de seus filhos. Por meio de contribuições
                    livres, os responsáveis financeiros podem poupar dinheiro suficiente para arcar com o estudo de seus
                    dependentes.
                </p>
                <p>2007 – Em 31 de julho de 2007 a SulAmérica encerra a joint venture na Gerling Sul América S.A. –
                    Seguros Industriais entre a Gerling-Kozern e a Sul América Companhia Nacional de Seguros, que
                    adquiriu os restantes 43,82% do capital social total da Gerling Sul América S.A. – Seguros
                    Industriais, passando a deter a totalidade das suas ações. Em assembléia geral extraordinária de 8
                    de agosto de 2007, a razão social da Gerling Sul América S.A. – Seguros Industriais foi alterada
                    para Sul América Companhia de Seguros Gerais.
                </p>
                <p>2008 – Foi firmado contrato de parceria comercial com as sociedades do grupo Votorantim BV Financeira
                    S.A. Crédito, Financiamento e Investimento (“BV Financeira”), BV Leasing Arrendamento Mercantil S.A.
                    (“BV Leasing”) e VCS – Votorantim Corretora de Seguros Ltda. (“VCS”) para promoção do seguro
                    SulAmérica Auto em toda a rede da BV Financeira e da BV Leasing, em caráter de exclusividade. O
                    acordo é válido por cinco anos e permitirá que o seguro SulAmérica Auto seja comercializado, por
                    meio da VCS, na rede de pontos da BV Financeira e da BV Leasing, formada por mais de 18 mil revendas
                    de veículos onde as empresas têm representantes, em todo o país.
                </p>
            </div>
        </div>
        <hr class="tall"/>
    </div>

@endsection

@section('chamada')
    @include('skeleton.chamada', ['empresa' => 'SulAmerica'])
@endsection