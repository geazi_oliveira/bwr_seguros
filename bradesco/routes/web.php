<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return view('index');
});

$router->get('plano-de-saude', function() {
    return view('plano-saude');
});

$router->get('plano-empresarial', function() {
    return view('plano-empresarial');
});

$router->get('plano-dental', function() {
    return view('plano-dental');
});


$router->get('orcamento', function() {
    return view('orcamento');
});
