@extends('default')
@section('content')
    @include('skeleton.breadcrumb', ['name' => 'Plano de Saúde'])

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-4">
                        <div class="feature-box secundary">
                            <div class="feature-box-icon">
                                <i class="fa fa-shopping-bag"></i>
                            </div>
                            <div class="feature-box-info">
                                <h4 class="shorter">Clube de Vantagens</h4>
                                <p class="tall">
                                    Os cliente Bradesco Saúde podem se cadastrar e aproveitar os benefícios de cerca de
                                    250 parceiros com 946 estabelecimentos e cobertura nacional pelo site:
                                    <a href="clubedevantagens.bradescoseguros.com.br" target="_blank">
                                        clubedevantagens.bradescoseguros.com.br
                                    </a>
                                </p>
                            </div>
                        </div>
                        <div class="feature-box secundary push-top">
                            <div class="feature-box-icon">
                                <i class="fa fa-user-md"></i>
                            </div>
                            <div class="feature-box-info">
                                <h4 class="shorter">Segunda Opnião Médica</h4>
                                <p class="tall">
                                    Trata-se de uma consulta, sem custo, com um profissional especialista, com o
                                    objetivo de enriquecer o diagnóstico por meio de uma avaliação diferenciada.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="feature-box secundary">
                            <div class="feature-box-icon">
                                <i class="fa fa-hand-holding-heart"></i>
                            </div>
                            <div class="feature-box-info">
                                <h4 class="shorter">Transplantes</h4>
                                <p class="tall">
                                    De RIM, CÓRNEA e MEDULA ÓSSEA (autólogo e alogênico) listados no rol de
                                    procedimentos e eventos em saúde editado pela Agência Nacional de Saúde
                                    Suplementar (ANS) vigente à época do evento.
                                </p>
                            </div>
                        </div>
                        <div class="feature-box secundary push-top">
                            <div class="feature-box-icon">
                                <i class="fa fa-capsules"></i>
                            </div>
                            <div class="feature-box-info">
                                <h4 class="shorter">Desconto em Farmácia</h4>
                                <p class="tall">
                                    O Desconto Farmácia oferece mais de 4 mil medicamentos com até 60% de desconto, em
                                    cerca de 8 mil farmácias conveniadas em todo o Brasil.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="feature-box secundary">
                            <div class="feature-box-icon">
                                <i class="fa fa-user-md"></i>
                            </div>
                            <div class="feature-box-info">
                                <h4 class="shorter">Meu Doutor Bradesco</h4>
                                <p class="tall">
                                    O serviço está disponível nas especialidades de Clínica Médica*, Pediatria*,
                                    Cardiologia*, Traumato-ortopedia*, Coluna* e Diabetes**.
                                </p>
                            </div>
                        </div>
                        <div class="feature-box secundary push-top">
                            <div class="feature-box-icon">
                                <i class="fa fa-hospital-alt"></i>
                            </div>
                            <div class="feature-box-info">
                                <h4 class="shorter">NovaMed</h4>
                                <p class="tall">
                                    A Novamed é uma rede de clínicas médicas projetada para cuidar da saúde com padrão
                                    de atendimento diferenciado: além de oferecer atendimento ambulatorial primário em
                                    várias especialidades médicas, realiza consultas, exames de apoio diagnóstico e
                                    procedimentos cirúrgicos ambulatoriais.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('skeleton.parallax', [
        'title' => 'Diferenciais Bradesco Saúde',
        'lead' => 'Sinônimo de qualidade, credibilidade, solidez e segurança, o Plano de Saúde Bradesco está presente em todo o território nacional, com produtos criados para atender ao segmento empresarial da melhor forma possível. Conheça abaixo os diferenciais que o Plano de Saúde Bradesco oferece.',
        'tall' => ''

    ])

@endsection