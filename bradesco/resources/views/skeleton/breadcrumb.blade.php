<section class="page-top">
    <div class="slider-container">
        <div class="slider" id="revolutionSlider" data-plugin-revolution-slider data-plugin-options='{"startheight": 200}'>
            <ul>
                <li data-transition="fade" data-slotamount="13" data-masterspeed="300">
                    <img src="/img/header-default-bg.jpg" data-bgfit="cover" data-bgposition="left top" data-bgrepeat="no-repeat">
                </li>
            </ul>
        </div>
    </div>
    <div class="page-top-info container">
        <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumb">
                    <li>
                        <a href="/">Home</a>
                    </li>
                    <li class="active">{{ $name }}</li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <h2>{{ $name }}</h2>
            </div>
        </div>
    </div>
</section>