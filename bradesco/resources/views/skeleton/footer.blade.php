<footer id="footer">

    <div class="col-md-12">
        <div class="menu-contato">
            <div class="container">
                <div class="col-md-3 col-xs-3">
                    <ul>
                        <li>
                            <a href="whatsapp://send?phone=+5511954335433&text=Olá%20gostaria%20de%20informações%20sobre%20o%20plano%20de%20Saúde%20Empresarial%20Bradesco!" class="block-link">
                                <i class="fab fa-whatsapp"></i>
                                <br>
                                <strong>WHATSAPP</strong>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="col-md-3 col-xs-3">
                    <ul>
                        <li>
                            <a href="mailto:{{ $email }}" class="block-link">
                                <i class="fa fa-envelope"></i>
                                <br>
                                <strong>EMAIL</strong>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="col-md-3 col-xs-3">
                    <ul>
                        <li>
                            <a href="tel:0800 580 23 85" class="block-link">
                                <i class="fa fa-phone"></i>
                                <br>
                                <strong>TELEFONE</strong>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="container bottom-footer" style="height: 40px">
        <div class="row" style="height: 40px">
            <div class="col-md-12" style="height: 40px">


                <div class="contact-details">
                    <ul class="contact">
                        <li style="float: left;">
                            <p>
                                <i class="fa fa-map-marker"></i>
                                <strong>Localização:</strong> Rua Itapura, 239 conjunto 503 - Tatuapé São Paulo</p>
                        </li>
                        <li style="float: left;padding-left: 20px;">
                            <p>
                                <i class="fa fa-phone"></i>
                                <strong>Telefone:</strong>(11) 4117-3781 / (11) 95433-5433 tim (whatsapp) / 0800 580 23 85</p>
                        </li>
                        <li style="float: left;padding-left: 20px;">
                            <p>
                                <i class="fa fa-envelope"></i>
                                <strong>Email:</strong>
                                <a href="mailto:{{ $email }}">{{ $email }}</a>
                            </p>
                        </li>
                    </ul>
                </div>

            </div>
        </div>
    </div>
</footer>