<div class="container">

    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-4">
                    <div class="feature-box secundary">
                        <div class="feature-box-icon">
                            <i class="fa fa-users"></i>
                        </div>
                        <div class="feature-box-info">
                            <h4 class="shorter">SPG | Plano Premium</h4>
                            <p class="tall">
                                Ideal para empresas que desejam oferecer cuidados e exclusividade aos seus funcionários,
                                com atendimento diferenciado em todas as regiões do Brasil. Possui serviços e coberturas
                                especiais, que vão além do cuidado com a saúde do beneficiário. Com uma exclusiva rede
                                de referenciados, contempla hospitais que são referência em serviços de saúde no país.
                            </p>
                        </div>
                    </div>
                    <div class="feature-box secundary push-top">
                        <div class="feature-box-icon">
                            <i class="fa fa-users"></i>
                        </div>
                        <div class="feature-box-info">
                            <h4 class="shorter">SPG | Plano Nacional</h4>
                            <p class="tall">
                                Aproveite a comodidade de atendimento em todas as regiões do Brasil e as vantagens de
                                uma ampla rede de referenciados. Com este plano, os colaboradores da sua empresa podem
                                escolher o melhor padrão de reembolso, que está disponível para atendimentos no Brasil
                                e no Exterior.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="feature-box secundary">
                        <div class="feature-box-icon">
                            <i class="fa fa-users"></i>
                        </div>
                        <div class="feature-box-info">
                            <h4 class="shorter">SPG | Plano Preferencial Plus</h4>
                            <p class="tall">
                                Uma ótima opção para a sua empresa, que busca um plano com cobertura regional. O plano
                                Preferencial Plus conta com atendimento nos principais municípios de São Paulo e Rio
                                de Janeiro, por meio de uma ampla rede de referenciados. Dentre seus benefícios estão
                                reembolso em caso de atendimento no Brasil e no Exterior.
                            </p>
                        </div>
                    </div>
                    <div class="feature-box secundary push-top">
                        <div class="feature-box-icon">
                            <i class="fa fa-users"></i>
                        </div>
                        <div class="feature-box-info">
                            <h4 class="shorter">SPG | Plano Nacional Plus</h4>
                            <p class="tall">
                                O plano oferece atendimento diferenciado, conforto e qualidade, em todas as regiões do
                                país, especialmente para empresas que buscam uma rede referenciada exclusiva. Conta
                                com hospitais que são referência em serviços de saúde no país e possui flexibilidade
                                na escolha de padrão de reembolso - que é válido para atendimento no Brasil e no
                                exterior.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="feature-box secundary">
                        <div class="feature-box-icon">
                            <i class="fa fa-users"></i>
                        </div>
                        <div class="feature-box-info">
                            <h4 class="shorter">SPG | Plano Nacional Flex</h4>
                            <p class="tall">
                                Com este plano os funcionários da sua empresa contam com atendimento em todas as regiões
                                do Brasil, por valores acessíveis, a partir de uma rede integrada de serviços, adequada
                                às necessidades dos beneficiários. Também oferece opção de reembolso para atendimentos
                                nacionais.
                            </p>
                        </div>
                    </div>
                    <div class="feature-box secundary push-top">
                        <div class="feature-box-icon">
                            <i class="fa fa-users"></i>
                        </div>
                        <div class="feature-box-info">
                            <h4 class="shorter">SPG | Plano Perfil</h4>
                            <p class="tall">
                                Este plano oferece soluções personalizadas, de acordo com as necessidades do seu negócio.
                                Com atendimento regionalizado, oferece a melhor relação custo-benefício e uma rede
                                integrada de serviços adequada às necessidades dos beneficiários. Dentre os seus
                                diferenciais estão cobertura de urgência e emergência nas principais capitais do país.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>