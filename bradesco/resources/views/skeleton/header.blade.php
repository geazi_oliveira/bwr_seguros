<header id="header">
    <div class="container">
        <h1 class="logo">
            <a href="/">
                <img alt="Tucson" width="148" data-sticky-width="134" data-sticky-height="40" src="/img/logo.png">
            </a>
        </h1>
        <button class="btn btn-responsive-nav btn-inverse" data-toggle="collapse" data-target=".nav-main-collapse">
            <i class="fa fa-bars"></i>
        </button>
    </div>
    <div class="navbar-collapse nav-main-collapse collapse">
        <div class="container">
            <nav class="nav-main mega-menu">
                <ul class="nav nav-pills nav-main" id="mainMenu">
                    <li class="{{ \Illuminate\Support\Facades\Request::is('/') ? 'active' : '' }}">
                        <a href="/">Home</a>
                    </li>
                    <li class="{{ \Illuminate\Support\Facades\Request::is('plano-de-saude') ? 'active' : '' }}">
                        <a href="/plano-de-saude">Planos de Saúde</a>
                    </li>
                    <li class="{{ \Illuminate\Support\Facades\Request::is('plano-empresarial') ? 'active' : '' }}">
                        <a href="/plano-empresarial">Planos Empresariais</a>
                    </li>
                    <li class="{{ \Illuminate\Support\Facades\Request::is('plano-dental') ? 'active' : '' }}">
                        <a href="/plano-dental">Bradesco Dental</a>
                    </li>
                    <li class="{{ \Illuminate\Support\Facades\Request::is('orcamento') ? 'active' : '' }}">
                        <a href="/orcamento">Solicite um Orçamento</a>
                    </li>
                </ul>
            </nav>
        </div>
    </div>
</header>