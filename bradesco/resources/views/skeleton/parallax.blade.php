<section class="parallax" data-stellar-background-ratio="0.4" style="background-image: url(img/parallax.jpg);">
    <div class="container">
        <div class="row">
            <div class="col-md-6" data-appear-animation="fadeInLeft">
                <h2 class="push-top">
                    <i class="fa fa-star-o"></i>
                    <strong>{{ $title }}</strong>
                </h2>
                <p class="red-b lead">
                    {{ $lead }}
                <p class="red-b tall">
                    {{ $tall }}
                </p>

                <a class="btn btn-primary btn-lg push-top" href="/orcamento">Fazer um Orçamento</a>

            </div>

            <div class="right-side col-md-6" style="margin-top: 150px">
                <img class="img-responsive pull-right" src="/img/bradesco/medico.png">
            </div>

        </div>

    </div>
</section>