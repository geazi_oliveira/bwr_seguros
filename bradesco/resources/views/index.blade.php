@extends('default')
@section('content')
    <section class="home-top clean">
        <div class="slider-container">
            <div class="slider" id="revolutionSlider" data-plugin-revolution-slider data-plugin-options='{"startheight": 580}'>
                <ul>
                    <li data-transition="fade" data-slotamount="13" data-masterspeed="300">
                        <img src="/img/slides/banner1.jpg" data-bgposition="left top" data-bgrepeat="no-repeat">
                    </li>
                    <li data-transition="fade" data-slotamount="13" data-masterspeed="300">
                        <img src="/img/slides/banner2.jpg" data-bgposition="left top" data-bgrepeat="no-repeat">
                    </li>
                </ul>
            </div>
        </div>
    </section>

    @include('skeleton.chamada')
    @include('skeleton.parallax', [
        'title' => 'O Melhor Seguro Saúde',
        'lead' => 'Você poderá escolher com quem e onde se tratar. Neste caso, você paga as despesas e depois apresenta à Bradesco Saúde a documentação exigida.',
        'tall' => 'O reembolso na Livre Escolha é feito dentro dos limites da Tabela de Honorários e Serviços Médicos e da Tabela de Serviços
                    Hospitalares dO Bradesco Saúde. Presente em todo o Brasil desde 1984, a empresa possui planos de saúde e planos
                    odontológicos com uma ampla rede referenciada. Buscamos oferecer o melhor atendimento e os mais completos serviços
                    que você pode desejar em um plano de saúde ou dental para você, para sua família e para sua empresa. Atualmente,
                    O Bradesco Seguros conta com cerca de 4,2 milhões de segurados, sendo que, destes, mais de 96% são beneficiários
                    de planos coletivos.'

    ])

@endsection