@extends('default')
@section('content')
    @include('skeleton.breadcrumb', ['name' => 'Plano de Empresarial'])

    @include('skeleton.chamada')
    @include('skeleton.parallax', [
        'title' => 'Bradesco Saúde Empresarial',
        'lead' => 'O Plano de Saúde Bradesco Empresarial disponibiliza um produto voltado para o atendimento de micro, pequenas e médias empresas que valorizam a qualidade de vida de seus colaboradores e dependentes: o Bradesco Saúde SPG (seguro para grupos – 3 a 199 vidas). O produto oferece grande flexibilidade na contratação e preços competitivos.',
        'tall' => ''

    ])

@endsection