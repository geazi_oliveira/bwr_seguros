@extends('default')
@section('content')
    @include('skeleton.breadcrumb', ['name' => 'Bradesco Dental'])


    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2>Plano Odontológico</h2>
                <p>
                    Os planos odontológicos Bradesco Dental têm origem na integração das atividades desenvolvidas pelo
                    Bradesco Dental e OdontoPrev, no ramo de assistência odontológica. Em outubro de 2009 foi celebrado
                    Acordo de Associação entre a OdontoPrev, a maior operadora de planos odontológicos da América
                    Latina, e O Bradesco Dental. Desde Julho de 2010, por meio da incorporação societária, O Bradesco
                    Dental passou a ser uma marca do Grupo OdontoPrev e O Bradesco Saúde S.A. (Bradesco Saúde),
                    acionista do mesmo. O Grupo possui cerca de 6,4 milhões de beneficiários e conta com dentistas
                    e clínicas credenciadas em todo o Brasil. Seus serviços atendem a milhares de clientes corporativos,
                    incluindo desde micro e pequenas empresas até grandes organizações – com destaque para empresas
                    líderes de diversos segmentos da indústria, comércio, serviços e agronegócio.
                </p>

            </div>
        </div>

        <hr class="tall">

        <div class="row">
            <div class="col-md-12">
                <h2>Plano Odontológico</h2>
                <div class="row">
                    <div class="col-md-4">
                        <div class="feature-box secundary">
                            <div class="feature-box-icon">
                                <i class="fa fa-hospital"></i>
                            </div>
                            <div class="feature-box-info">
                                <h4 class="shorter">Consultas</h4>
                            </div>
                        </div>
                        <div class="feature-box secundary push-top">
                            <div class="feature-box-icon">
                                <i class="fa fa-ambulance"></i>
                            </div>
                            <div class="feature-box-info">
                                <h4 class="shorter">Urgências</h4>
                            </div>
                        </div>
                        <div class="feature-box secundary push-top">
                            <div class="feature-box-icon">
                                <i class="fa fa-x-ray"></i>
                            </div>
                            <div class="feature-box-info">
                                <h4 class="shorter">Radiologia</h4>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="feature-box secundary">
                            <div class="feature-box-icon">
                                <i class="fa fa-tooth"></i>
                            </div>
                            <div class="feature-box-info">
                                <h4 class="shorter">Dentística</h4>
                            </div>
                        </div>
                        <div class="feature-box secundary push-top">
                            <div class="feature-box-icon">
                                <i class="fa fa-screwdriver"></i>
                            </div>
                            <div class="feature-box-info">
                                <h4 class="shorter">Endodontia</h4>
                            </div>
                        </div>
                        <div class="feature-box secundary push-top">
                            <div class="feature-box-icon">
                                <i class="fa fa-tooth"></i>
                            </div>
                            <div class="feature-box-info">
                                <h4 class="shorter">Prótese</h4>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="feature-box secundary">
                            <div class="feature-box-icon">
                                <i class="fa fa-user-md"></i>
                            </div>
                            <div class="feature-box-info">
                                <h4 class="shorter">Cirurgia</h4>
                            </div>
                        </div>
                        <div class="feature-box secundary push-top">
                            <div class="feature-box-icon">
                                <i class="fa fa-tooth"></i>
                            </div>
                            <div class="feature-box-info">
                                <h4 class="shorter">Periodontia</h4>
                            </div>
                        </div>
                        <div class="feature-box secundary push-top">
                            <div class="feature-box-icon">
                                <i class="fa fa-tooth"></i>
                            </div>
                            <div class="feature-box-info">
                                <h4 class="shorter">Odontopediatria</h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

    @include('skeleton.parallax', [
        'title' => 'Bradesco Dental',
        'lead' => 'Ir regularmente ao dentista traz mais qualidade de vida para você e para a sua família. Não só por razões estéticas: quando cuidamos do nosso sorriso, também prevenimos uma série de outras doenças.',
        'tall' => 'Uma dor de dente ou outro problema bucal inesperado pode prejudicar seu orçamento familiar. Nossos planos odontológicos te ajudam nestes momentos inesperados.'

    ])

@endsection