<div class="container">

    <div class="row">

        <div data-appear-animation="fadeInDown" class="appear-animation">
            <div class="row featured-boxes secundary">
                <div class="col-md-2">
                    <div data-appear-animation="bounceIn" data-appear-animation-delay="600" class="appear-animation">
                        <div class="featured-box featured-box-primary">
                            <div class="box-content">
                                <i class="icon-featured fa fa-check"></i>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-10">
                    <h3 class="short dark">Amil 200</h3>
                    <p>
                        O Amil 200 é um dos planos mais comercializados do Grupo Amil, tanto pela extensa rede de
                        atendimentos quanto por ser um plano regional, ou seja, o seu atendimento engloba todo o
                        estado de São Paulo.
                    </p>
                </div>
            </div>
        </div>

        <div data-appear-animation="fadeInDown" class="appear-animation">
            <div class="row">
                <div data-appear-animation="fadeIn" data-appear-animation-delay="800" class="appear-animation">
                    <div class="col-md-2 center push-bottom">
									<span class="icon-rounded">
										<i class="fas fa-plus"></i>
									</span>
                    </div>
                </div>
            </div>
        </div>

        <div data-appear-animation="fadeInDown" class="appear-animation">
            <div class="row featured-boxes secundary">
                <div class="col-md-2">
                    <div data-appear-animation="bounceIn" data-appear-animation-delay="600" class="appear-animation">
                        <div class="featured-box featured-box-primary">
                            <div class="box-content">
                                <i class="icon-featured fa fa-check"></i>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-10">
                    <h3 class="short dark">Amil 400</h3>
                    <p>
                        A Amil 400 é a primeira categoria que atende a nível nacional com custo acessível e total respeito ao cliente.
                    </p>
                </div>
            </div>
        </div>

        <div data-appear-animation="fadeInDown" class="appear-animation">
            <div class="row">
                <div data-appear-animation="fadeIn" data-appear-animation-delay="800" class="appear-animation">
                    <div class="col-md-2 center push-bottom">
									<span class="icon-rounded">
										<i class="fas fa-plus"></i>
									</span>
                    </div>
                </div>
            </div>
        </div>

        <div data-appear-animation="fadeInDown" class="appear-animation">
            <div class="row featured-boxes secundary">
                <div class="col-md-2">
                    <div data-appear-animation="bounceIn" data-appear-animation-delay="600" class="appear-animation">
                        <div class="featured-box featured-box-primary">
                            <div class="box-content">
                                <i class="icon-featured fa fa-check"></i>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-10">
                    <h3 class="short dark">Amil 500</h3>
                    <p>
                        O Amil 500 com abrangência nacional conta com uma rede credenciada maior e hospitais de referencia no mercado – Ex: Hospital 9 DE JULHO, Santa Joana, entre outros...
                    </p>
                </div>
            </div>
        </div>

        <div data-appear-animation="fadeInDown" class="appear-animation">
            <div class="row">
                <div data-appear-animation="fadeIn" data-appear-animation-delay="800" class="appear-animation">
                    <div class="col-md-2 center push-bottom">
									<span class="icon-rounded">
										<i class="fas fa-plus"></i>
									</span>
                    </div>
                </div>
            </div>
        </div>

        <div data-appear-animation="fadeInDown" class="appear-animation">
            <div class="row featured-boxes secundary">
                <div class="col-md-2">
                    <div data-appear-animation="bounceIn" data-appear-animation-delay="600" class="appear-animation">
                        <div class="featured-box featured-box-primary">
                            <div class="box-content">
                                <i class="icon-featured fa fa-check"></i>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-10">
                    <h3 class="short dark">Amil 700</h3>
                    <p>
                        O Amil 700 é um dos planos de saúde mais procurados, possui uma ampla rede credenciada e atendimento de alto padrão, é o melhor dos produtos da Amil Blue por trabalhar com hospitais e laboratórios renomados em todo o país. Ex de Hospitais: Rede São Luiz / Hospital Samaritano / Oswaldo Cruz entre outros...
                    </p>
                </div>
            </div>
        </div>

        <div data-appear-animation="fadeInDown" class="appear-animation">
            <div class="row">
                <div data-appear-animation="fadeIn" data-appear-animation-delay="800" class="appear-animation">
                    <div class="col-md-2 center push-bottom">
									<span class="icon-rounded">
										<i class="fas fa-plus"></i>
									</span>
                    </div>
                </div>
            </div>
        </div>

        <div data-appear-animation="fadeInDown" class="appear-animation">
            <div class="row featured-boxes secundary">
                <div class="col-md-2">
                    <div data-appear-animation="bounceIn" data-appear-animation-delay="600" class="appear-animation">
                        <div class="featured-box featured-box-primary">
                            <div class="box-content">
                                <i class="icon-featured fa fa-check"></i>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-10">
                    <h3 class="short dark">Amil 900</h3>
                    <p>
                        O plano Amil 900 é um dos planos que oferece o melhor atendimento dos planos de saúde empresariais de todo o Brasil com atendimento nos melhores hospitais como: Hospital Israelita Albert Einstein, Hospital Sírio Libanês, Hospital Alemão Oswaldo Cruz, Grupo D’or São Luiz entre outros e laboratórios como: Fleury, Salomão & Zoppi, Cura, Delboni Auriemo, A+ Medicina Diagnóstica, Albert Einstein entre outros.
                    </p>
                </div>
            </div>
        </div>

    </div>
    <hr class="tall"/>
</div>