@extends('default')
@section('content')
    @include('skeleton.breadcrumb', ['name' => 'Plano de Saúde'])

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2>Linha Amil</h2>

                <p>
                    A linha Amil é uma linha de planos de saúde para você que procura o melhor custo x benefício na contratação do seu plano
                    de saúde. Com o padrão de atendimento Amil, os planos de saúde Amil oferecem a seus clientes
                    o que há de mais moderno em medicina, oferece uma rede credenciada de médicos, hospitais, clinicas
                    e laboratórios com excelentes serviços e coberturas em todas as regiões.
                </p>

            </div>
        </div>
        <hr class="tall"/>
        <div class="row">
            <div class="col-md-12">
                <h2>Para quem são os Planos Amil ?</h2>

                <p>
                    A amil oferece diversas modalidades de planos para micro empresas, empresas a partir de 2 vidas e para grandes empresas a partir de 99 vidas.
                </p>

            </div>
        </div>
        <hr class="tall"/>
        <div class="row">
            <div class="col-md-12">
                <h2>Porque escolher os planos Amil?</h2>

                <p>
                    Os planos da linha Amil oferecem a melhor relação custo benefício dentre os planos Amil. Oferecem rede credenciada própria
                    com a mesma qualidade dos outros produtos da linha Amil Saúde.
                </p>

            </div>
        </div>
        <hr class="tall"/>
    </div>

@endsection

@section('chamada')
    @include('skeleton.chamada', ['empresa' => 'Amil'])
@endsection