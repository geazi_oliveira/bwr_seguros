@extends('default')
@section('content')
    <section class="home-top clean">
        <div class="slider-container">
            <div class="slider" id="revolutionSlider" data-plugin-revolution-slider data-plugin-options='{"startheight": 580}'>
                <ul>
                    <li data-transition="fade" data-slotamount="13" data-masterspeed="300">
                        <img src="/img/slides/banner1.jpg" data-bgposition="left top" data-bgrepeat="no-repeat">

                        <div class="tp-caption white top-label md white customin" data-x="left" data-hoffset="115" data-y="170" data-customin="x:0;y:0;z:0;rotationX:90;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
                             data-speed="800" data-start="0" data-easing="Back.easeInOut" data-endspeed="300">Precisando de um plano empresarial
                        </div>

                        <div class="tp-caption main-label md white customin" data-x="left" data-hoffset="115" data-y="190" data-customin="x:0;y:0;z:0;rotationX:90;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
                             data-speed="800" data-start="300" data-easing="Back.easeInOut" data-endspeed="300">AMIL
                        </div>

                        <div class="tp-caption bottom-label md white sfb" data-x="left" data-hoffset="135" data-y="290" data-speed="800" data-start="500"
                             data-easing="Back.easeInOut" data-endspeed="300">Conheça agora mesmo
                        </div>

                    </li>
                    <li data-transition="fade" data-slotamount="13" data-masterspeed="300">
                        <img src="/img/slides/banner2.jpg" data-bgposition="left top" data-bgrepeat="no-repeat">
                    </li>
                </ul>
            </div>
        </div>
    </section>

    @include('skeleton.cahamada-logo')

    @include('skeleton.parallax')

    @include('skeleton.planos')

@endsection