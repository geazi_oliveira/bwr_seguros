@extends('default')
@section('content')
    @include('skeleton.breadcrumb', ['name' => 'Plano Dental'])

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2>Amil Dental</h2>

                <p>
                    Somente na Amil Dental você faz a contratação de seu plano odontológico 100% on-line, de forma rápida,
                    segura e sem burocracia, com opção de pagamento no boleto à vista ou no cartão de crédito em até 12
                    parcelas, com desconto de 15% e isenção de carências ou ainda, através de boleto mensal. Para ter
                    saúde total você precisa estar em dia com sua saúde bucal. Além disso, ter um sorriso bonito
                    clareia muitas oportunidades em sua vida pessoal e profissional.
                </p>

            </div>
        </div>
        <hr class="tall"/>
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-6">
                        <div class="feature-box secundary">
                            <div class="feature-box-icon">
                                <i class="fa fa-x-ray"></i>
                            </div>
                            <div class="feature-box-info">
                                <h4 class="shorter">Raios X (panorâmicos e periapicais)</h4>
                            </div>
                        </div>
                        <div class="feature-box secundary push-top">
                            <div class="feature-box-icon">
                                <i class="fa fa-user-md"></i>
                            </div>
                            <div class="feature-box-info">
                                <h4 class="shorter">Consultas (inclusive aos sábados)</h4>
                            </div>
                        </div>
                        <div class="feature-box secundary push-top">
                            <div class="feature-box-icon">
                                <i class="fa fa-tooth"></i>
                            </div>
                            <div class="feature-box-info">
                                <h4 class="shorter">Próteses</h4>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="feature-box secundary">
                            <div class="feature-box-icon">
                                <i class="fa fa-tooth"></i>
                            </div>
                            <div class="feature-box-info">
                                <h4 class="shorter">Tratamento de canal (endodontia)</h4>
                            </div>
                        </div>
                        <div class="feature-box secundary push-top">
                            <div class="feature-box-icon">
                                <i class="fa fa-ambulance"></i>
                            </div>
                            <div class="feature-box-info">
                                <h4 class="shorter">Clínicas de urgência 24 horas</h4>
                            </div>
                        </div>
                        <div class="feature-box secundary push-top">
                            <div class="feature-box-icon">
                                <i class="fa fa-tooth"></i>
                            </div>
                            <div class="feature-box-info">
                                <h4 class="shorter">Instalação e manutenção de aparelhos</h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <hr class="tall"/>
    </div>

@endsection

@section('chamada')
    @include('skeleton.chamada', ['empresa' => 'Amil'])
@endsection