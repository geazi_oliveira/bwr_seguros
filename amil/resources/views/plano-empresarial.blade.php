@extends('default')
@section('content')
    @include('skeleton.breadcrumb', ['name' => 'Plano de Empresarial'])

    @include('skeleton.cahamada-logo')

    @include('skeleton.parallax')

    @include('skeleton.planos')

@endsection